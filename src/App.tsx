import List from "./components/List"
import "./App.css"
import { Provider } from "react-redux";
import store from "./store";
import { useState } from "react";

function App() {
  const [spinnerOff, turnOff] = useState(false)
  setTimeout(() => {
    turnOff(true)
  }, 3000)
  return (
    <Provider store={store}>
      <div className="App">
        {!spinnerOff && <div className="loader"></div>}
        {spinnerOff && <>
          <div className="banner">SHOPPING LIST</div>
          <List />
        </>}
      </div>


    </Provider>
  );
}

export default App;
