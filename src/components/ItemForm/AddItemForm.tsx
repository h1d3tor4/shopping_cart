import "./ItemForm.css"
import React, { MouseEventHandler, useEffect, useRef, useState } from "react";
import { CgPushChevronRight } from "react-icons/cg"
import { useAppDispatch } from "../../store.hooks";
import { addItem } from "../ListItem/item.slice";

interface FormProps {
    hideForm: Function
}

function AddItemForm({ hideForm }: FormProps) {
    const ref = useRef()
    const dispatch = useAppDispatch()


    //local state for item
    const [item, setItem] = useState({
        title: '',
        description: '',
        quantity: 0,
        selected: false,
        id: "",
        cost: 0.00
    })
    const [count, setCount] = useState(0)

    //handle changes in form and update state
    const handleChange = ({ target: { name, value } }: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => setItem(prev => {
        (prev as any)[name] = value;
        const newValue = { ...prev }
        if (typeof newValue.quantity === 'string') {
            newValue.quantity = parseInt(newValue.quantity)
        }

        if (typeof newValue.cost === 'string') {

            newValue.cost = parseFloat(newValue.cost)
        }

        if (name === "description") {
            setCount(value.length)
        }
        return newValue
    })
    //dispatch new item to the store and hide form
    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault()

        if (quantity === 0) {
            alert("Please select how many to add.")
        } else {
            item.id = item.title.replaceAll(" ", "") + Math.floor(Math.random() * 1000000000)
            dispatch(addItem(item))
            hideForm()
        }

    }

    const generateOptions = () => {
        const optionReturn = []
        for (var i = 1; i < 26; i++) {
            optionReturn.push(<option className="option" key={i} value={i}>{i}</option>)
        }
        return optionReturn;
    }

    //ease of readibility
    const { title, description, quantity, cost } = item

    return (

        <div id="id" className="ItemFormBase">
            <div onClick={() => { hideForm() }} id="fill"></div>
            <div className="ItemForm">
                <div className="header">
                    <div className="header-title">SHOPPING LIST</div>
                    <div className="return"><CgPushChevronRight size="20" /></div>
                </div>

                <div className="form-title">Add an Item</div>
                <div className="form-description">Add your new item below</div>
                <form onSubmit={handleSubmit}>
                    <input required type="text" placeholder="Item Name" name="title" value={title} onChange={handleChange} />
                    <textarea required rows={10} placeholder="Description" name="description" maxLength={100} value={description} onChange={handleChange} />
                    Cost<input type="number" step="0.01" name="cost" placeholder="$$$" onChange={handleChange} />
                    <p className="count">{count + " / 100"}</p>
                    Quantity<select className="select" required defaultValue="default" name="quantity" onChange={handleChange}>
                        <option value="default" hidden>How many?</option>
                        {generateOptions()}
                    </select>
                    <button className="cancel" onClick={() => { hideForm() }} type="button">Cancel</button>
                    <button className="confirm" type="submit">Add Item</button>
                </form>
            </div>
        </div>

    );
}

export default AddItemForm;
