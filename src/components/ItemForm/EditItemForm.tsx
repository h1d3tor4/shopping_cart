import "./ItemForm.css"
import React, { useState } from "react";
import { CgPushChevronRight } from "react-icons/cg"
import { useAppDispatch } from "../../store.hooks";
import { editAnItem } from "../ListItem/item.slice";
import { Item } from "../../types/types";

interface FormProps {
    hideForm: Function,
    editItem: Item
}

function EditItemForm({ hideForm, editItem }: FormProps) {

    const dispatch = useAppDispatch()

    const [item, updateItem] = useState({
        title: '',
        description: '',
        quantity: 0,
        selected: false,
        id: "",
        cost: 0
    })
    const [count, setCount] = useState(editItem.description.length)


    //handle changes in form and update state
    const handleChange = ({ target: { name, value } }: React.ChangeEvent<HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement>) => updateItem(prev => {
        (prev as any)[name] = value;

        if (name === "description") {
            setCount(value.length)
        }

        const newValue = { ...prev }
        return newValue
    })
    //dispatch new item to the store and hide form
    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault()
        if (item.description === "") item.description = editItem.description
        if (item.title === "") item.title = editItem.title
        if (!item.quantity) item.quantity = editItem.quantity
        if (!item.cost) item.cost = editItem.cost


        item.id = editItem.id
        dispatch(editAnItem(item))
        hideForm()
    }

    const generateOptions = () => {
        const optionReturn = []
        for (var i = 1; i < 26; i++) {
            optionReturn.push(<option key={i} value={i}>{i}</option>)
        }
        return optionReturn;
    }


    return (

        <div className="ItemFormBase">
            <div onClick={() => { hideForm() }} id="fill"></div>
            <div className="ItemForm">
                <div className="header">
                    <div className="header-title">SHOPPING LIST</div>
                    <div className="return"><CgPushChevronRight size="20" /></div>
                </div>

                <div className="form-title">Edit Your Item</div>
                <div className="form-description">Add your new item below</div>
                <form onSubmit={handleSubmit}>
                    <input type="text" name="title" defaultValue={editItem.title} onChange={handleChange} />
                    <textarea rows={10} name="description" maxLength={100} defaultValue={editItem.description} onChange={handleChange} />
                    Cost<input type="number" step="0.01" name="cost" defaultValue={editItem.cost} onChange={handleChange} />
                    <p className="count">{count + " / 100"}</p>
                    Quantity
                    <select defaultValue={editItem.quantity} name="quantity" onChange={handleChange}>
                        <option value={editItem.quantity} hidden>{editItem.quantity}</option>
                        {generateOptions()}
                    </select>
                    <button className="cancel" onClick={() => { hideForm() }} type="button">Cancel</button>
                    <button className="confirm" type="submit">Update Item</button>
                </form>
            </div>
        </div>

    );
}

export default EditItemForm;
