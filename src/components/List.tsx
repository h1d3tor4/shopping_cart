import { useEffect, useState } from 'react';
import './List.css';
import { Item } from "../types/types"
import { useSelector } from 'react-redux';
import { getItemsSelector, removeItem } from './ListItem/item.slice';
import ListItem from './ListItem/ListItem';
import AddItemForm from './ItemForm/AddItemForm';
import EditItemForm from './ItemForm/EditItemForm';
import Warning from "./ListItem/Warning"
import { useAppDispatch } from '../store.hooks';

function List() {

    //Selector Calls
    const shopping_list = useSelector(getItemsSelector)

    //UseState Calls
    const [addItem, showAddItem] = useState(false)
    const [editItem, setEditItem] = useState<Item>()
    const [warn, showWarning] = useState("")
    const [estimateTotal, setEstimate] = useState(0)
    const [total, setTotal] = useState(0)


    //Dispatches
    const dispatch = useAppDispatch()
    const removeItemFromStore = (id: string) => {
        dispatch(removeItem(id))
    }

    //utility functions
    const hideForm = () => {
        showAddItem(false)
        setEditItem(undefined)
    }

    const hideWarning = () => {
        showWarning("")
    }

    const acceptWarning = () => {
        removeItemFromStore(warn)
        showWarning("")
    }

    const openEditForm = (item: Item) => {
        setEditItem(item)
    }

    useEffect(() => {
        let totals = 0;
        let estimate = shopping_list.map((item: Item) => {
            if (item.selected) totals += item.cost * item.quantity
            return item.cost * item.quantity
        })
        const sum = estimate.reduce((partialSum, a) => partialSum + a, 0).toFixed(2);
        setEstimate(parseFloat(sum))
        setTotal(totals)

    }, [shopping_list])


    //render
    return (
        <div className="List">

            {
                //If no list items display empty list screen with add first item button
            }
            {shopping_list.length < 1 && <div className="Empty-List">
                <h3>Your shopping list is empty :(</h3>
                <button onClick={() => { showAddItem(true) }} className="add-item" type="button">Add your first item</button>
            </div>}

            {
                //If list items show list screen with header and add item button
            }

            {shopping_list.length > 0 && <div className='item-list'>
                <div className='header'>
                    <div className='list-title'>Your Items</div>
                    <button onClick={() => { showAddItem(true) }} className="add-item" type="button">Add Item</button>
                </div>

                {shopping_list.map((listItem: Item, i: number) => {
                    return <ListItem showWarning={showWarning} openEdit={openEditForm} item={listItem} key={listItem.title + i} />
                })}
            </div>}
            {
                //Conditional Render for Warning
            }
            {warn.length > 0 && <Warning accept={acceptWarning} deny={hideWarning} />}


            {
                //Conditionally render Add item
            }

            {
                addItem && <AddItemForm hideForm={hideForm} />
            }

            {
                //Conditionally render Add item
            }

            {
                editItem && <EditItemForm editItem={editItem} hideForm={hideForm} />
            }
            <div className='total-cost'>Total Cost: ${total.toFixed(2)}</div>
            <div className='estimated-cost'>Estimated Cost: ${estimateTotal.toFixed(2)}</div>
        </div>
    );
}

export default List;
