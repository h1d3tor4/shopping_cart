import "./ListItem.css"
import { Item } from "../../types/types"
import { MdOutlineModeEditOutline, MdOutlineDeleteOutline } from "react-icons/md"
import { checkAnItem } from "./item.slice";
import { useAppDispatch } from "../../store.hooks";

interface listItem {
    item: Item,
    openEdit: Function,
    showWarning: Function
}

function ListItem({ item, openEdit, showWarning }: listItem) {
    const dispatch = useAppDispatch()
    const setChecked = () => {
        dispatch(checkAnItem(item.id))
    }

    let addedStyle = (item.selected ? " checked" : "")

    return (
        <div className={"ListItem" + addedStyle}>

            <input onChange={() => { setChecked() }} type="checkbox" className="checkbox detail" checked={item.selected} />
            <div className="title detail">{item.title}</div>
            <div className="description detail">{item.description}</div>
            <div className="cost detail">{"Total: $" + (item.cost * item.quantity).toFixed(2)}</div>
            <div className="quantity detail">{"quantity: " + item.quantity}</div>
            <div onClick={() => { openEdit(item) }} className="edit detail"><MdOutlineModeEditOutline size="20" /></div>
            <div onClick={() => { showWarning(item.id) }} className="delete detail"><MdOutlineDeleteOutline size="20" /></div>
        </div>
    );
}

export default ListItem;

