
import "./Warning.css"
import { Provider } from "react-redux";

interface WarningProps {
    accept: Function,
    deny: Function
}

function Warning({ accept, deny }: WarningProps) {
    return (

        <div className="WarningBase">
            <div id="fill" onClick={() => { deny() }}></div>
            <div className="Warning">
                <h2>Delete Item?</h2>
                <p>Are you sure you want to delete this item? This can not be undone.</p>
                <button onClick={() => { deny() }} className="Cancel">Cancel</button>
                <button onClick={() => { accept() }} className="Confirm">Confirm</button>
            </div>
        </div>

    );
}

export default Warning;
