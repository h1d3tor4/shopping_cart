import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../store";
import { Item } from "../../types/types"

const initialState: Item[] = []

const itemSlice = createSlice({
    name: 'items',
    initialState,
    reducers: {
        addItem: (state, action: PayloadAction<Item>) => {
            //return [action.payload, ...state]
            let i = state.findIndex(item => item.id === action.payload.id)
            if (state[i]) {
                let newstate = state
                newstate[i].quantity += action.payload.quantity
                return newstate
            } else {
                state.push(action.payload)
            }
        },
        removeItem: (state, action: PayloadAction<string>) => {
            return state.filter(item => item.id !== action.payload)
        },
        editAnItem: (state, action: PayloadAction<Item>) => {
            let i = state.findIndex(item => item.id === action.payload.id)
            let newstate = state
            newstate[i] = action.payload
            return newstate
        },
        checkAnItem: (state, action: PayloadAction<string>) => {
            let i = state.findIndex(item => item.id === action.payload)
            let newstate = state
            newstate[i].selected = !newstate[i].selected
            return newstate
        }
    }
})

export const { addItem, removeItem, editAnItem, checkAnItem } = itemSlice.actions;

export const getItemsSelector = (state: RootState) => state.items

export default itemSlice.reducer;