import { configureStore } from "@reduxjs/toolkit"
import { useDispatch } from "react-redux"
import items from "./components/ListItem/item.slice"

const store = configureStore({
    reducer: {
        items
    }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch


export default store;