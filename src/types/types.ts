export interface Item {
    title: string,
    selected: boolean,
    description: string,
    quantity: number,
    id: string,
    cost: number
}